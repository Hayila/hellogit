// ahh boy let's try this

#include <iostream>
#include <vector>
using namespace std;

int main()

{
	cout << "Let's do this!" << endl;
	vector<int> myVector;
	
	myVector.push_back(8);
	myVector.push_back(1);
	myVector.push_back(0);
	myVector.push_back(5);
	
	cout << "Vector: ";
	
	for (unsigned int i=0; i< myVector.size(); i++)
	{
		cout << myVector[i] << " ";
	}
	cout << endl;
	
	return 0;
}
